package discord

import (
	"encoding/json"
	"errors"
	"strings"
	"time"

	"golang.org/x/oauth2"
)

type OAuthSession interface {
	// GetAuthURL returns the URL for the authentication end-point for the provider.
	GetAuthURL() (string, error)
	// Marshal generates a string representation of the Session for storing between requests.
	Marshal() string
	// Authorize should validate the data from the provider and return an access token
	// that can be stored for later access to the provider.
	Authorize(OAuthProvider, Params) (string, error)
}

// TODO: Refactor into session.go
type Session struct {
	AuthURL      string
	AccessToken  string
	RefreshToken string
	ExpiresAt    time.Time
}

func (s Session) GetAuthURL() (string, error) {
	if s.AuthURL == "" {
		return "", errors.New("Missing AuthURL")
	}
	return s.AuthURL, nil
}

func (s *Session) Authorize(provider OAuthProvider, params Params) (string, error) {
	p := provider.(*Provider)
	token, err := p.config.Exchange(oauth2.NoContext, params.Get("code"))

	if err != nil {
		return "", err
	}

	if !token.Valid() {
		return "", errors.New("Invalid token received from discord")

	}

	s.AccessToken = token.AccessToken
	s.RefreshToken = token.RefreshToken
	s.ExpiresAt = token.Expiry
	return token.AccessToken, err
}

func (s Session) Marshal() string {
	j, _ := json.Marshal(s)
	return string(j)
}
func (s Session) String() string {
	return s.Marshal()
}
func (p *Provider) UnmarshalSession(data string) (OAuthSession, error) {
	s := &Session{}
	err := json.NewDecoder(strings.NewReader(data)).Decode(s)
	return s, err
}

// end session code //
