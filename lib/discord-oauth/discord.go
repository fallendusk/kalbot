package discord

import (
	"time"
)

const (
	authURL        string = "https://discordapp.com/api/oauth2/authorize"
	tokenURL       string = "https://discordapp.com/api/oauth2/token"
	userEndpoint   string = "https://discordapp.com/api/users/@me"
	guildsEndpoint string = "https://discordapp.com/api/users/@me/guilds"
)

const (
	ScopeIdentify    string = "identify"
	ScopeEmail       string = "email"
	ScopeConnections string = "connections"
	ScopeGuilds      string = "guilds"
	ScopeJoinGuild   string = "guilds.join"
	ScopeGroupDMjoin string = "gdm.join"
	ScopeBot         string = "bot"
	ScopeWebhook     string = "webhook.incoming"
)

type Params interface {
	Get(string) string
}

type User struct {
	RawData           map[string]interface{}
	Email             string
	Name              string
	Description       string
	UserID            string
	AvatarURL         string
	Location          string
	AccessToken       string
	AccessTokenSecret string
	RefreshToken      string
	ExpiresAt         time.Time
	Guilds            []Guild
}

// Guild is a struct for discord partial guild results
type Guild struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Icon        string `json:"icon"`
	Owner       bool   `json:"owner"`
	Permissions int    `json:"permissions"`
	Admin       bool
}

func New(clientKey string, secret string, callBackURL string, scopes ...string) *Provider {
	p := &Provider{
		ClientKey:   clientKey,
		Secret:      secret,
		CallbackURL: callBackURL,
	}
	p.config = newConfig(p, scopes)

	return p
}
