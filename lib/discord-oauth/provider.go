package discord

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"golang.org/x/oauth2"
)

type OAuthProvider interface {
	BeginAuth(state string) (OAuthSession, error)
	UnmarshalSession(string) (OAuthSession, error)
	FetchUser(OAuthSession) (User, error)
	RefreshToken(refreshToken string) (*oauth2.Token, error)
}

type Provider struct {
	ClientKey   string
	Secret      string
	CallbackURL string
	HTTPClient  *http.Client
	config      *oauth2.Config
}

func newConfig(p *Provider, scopes []string) *oauth2.Config {
	c := &oauth2.Config{
		ClientID:     p.ClientKey,
		ClientSecret: p.Secret,
		RedirectURL:  p.CallbackURL,
		Endpoint: oauth2.Endpoint{
			AuthURL:  authURL,
			TokenURL: tokenURL,
		},
		Scopes: []string{},
	}

	if len(scopes) > 0 {
		for _, scope := range scopes {
			c.Scopes = append(c.Scopes, scope)
		}
	} else {
		c.Scopes = []string{ScopeIdentify}
	}

	return c
}

func (p *Provider) Client() *http.Client {
	if p.HTTPClient != nil {
		return p.HTTPClient
	}
	return http.DefaultClient
}

func (p *Provider) BeginAuth(state string) (OAuthSession, error) {
	url := p.config.AuthCodeURL(state, oauth2.AccessTypeOnline)

	s := &Session{
		AuthURL: url,
	}
	return s, nil
}
func (p *Provider) fetchGuilds(u User) ([]Guild, error) {
	var guilds []Guild
	if u.AccessToken == "" {
		return guilds, fmt.Errorf("discord-oauth2: cannot get user guilds without accessToken")
	}

	req, err := http.NewRequest("GET", guildsEndpoint, nil)
	if err != nil {
		return guilds, err
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", "Bearer "+u.AccessToken)
	resp, err := p.Client().Do(req)
	if err != nil {
		if resp != nil {
			resp.Body.Close()
		}
		return guilds, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return guilds, fmt.Errorf("discord responded with Status %d trying to fetch user guilds", resp.StatusCode)
	}
	bits, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return guilds, err
	}

	err = json.NewDecoder(bytes.NewReader(bits)).Decode(&guilds)
	if err != nil {
		return guilds, err
	}

	for i, g := range guilds {
		guilds[i].Admin = hasAdminPermission(g.Permissions)
	}

	return guilds, err
}

func (p *Provider) FetchUser(session OAuthSession) (User, error) {
	s := session.(*Session)

	user := User{
		AccessToken:  s.AccessToken,
		RefreshToken: s.RefreshToken,
		ExpiresAt:    s.ExpiresAt,
	}

	if user.AccessToken == "" {
		return user, fmt.Errorf("discord-oauth2: cannot get user info without accessToken")
	}

	req, err := http.NewRequest("GET", userEndpoint, nil)
	if err != nil {
		return user, err
	}

	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", "Bearer "+s.AccessToken)
	resp, err := p.Client().Do(req)
	if err != nil {
		if resp != nil {
			resp.Body.Close()
		}
		return user, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return user, fmt.Errorf("discord responded with Status %d trying to fetch user information", resp.StatusCode)
	}

	bits, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return user, err
	}

	err = json.NewDecoder(bytes.NewReader(bits)).Decode(&user.RawData)
	if err != nil {
		return user, err
	}

	err = userFromReader(bytes.NewReader(bits), &user)
	if err != nil {
		return user, err
	}

	guilds, err := p.fetchGuilds(user)
	if err != nil {
		return user, err
	}
	user.Guilds = guilds
	return user, err
}

func userFromReader(r io.Reader, user *User) error {
	u := struct {
		Name          string `json:"username"`
		Email         string `json:"email"`
		AvatarID      string `json:"avatar"`
		MFAEnabled    bool   `json:"mfa_enabled"`
		Discriminator string `json:"discriminator"`
		Verified      bool   `json:"verified"`
		ID            string `json:"id"`
	}{}

	err := json.NewDecoder(r).Decode(&u)
	if err != nil {
		return err
	}

	user.Name = u.Name
	user.Email = u.Email
	user.AvatarURL = "https://media.discordapp.net/avatars/" + u.ID + "/" + u.AvatarID + ".jpg"
	user.UserID = u.ID

	return nil
}

func hasAdminPermission(permission int) bool {
	p := permission & 0x8
	if p == 0x8 {
		return true
	}
	return false
}

//func (p *DiscordProvider) FetchGuilds(session Session) (Guild[], error) {}

func (p *Provider) RefreshToken(refreshToken string) (*oauth2.Token, error) {
	token := &oauth2.Token{RefreshToken: refreshToken}
	ts := p.config.TokenSource(oauth2.NoContext, token)
	newToken, err := ts.Token()
	if err != nil {
		return nil, err
	}
	return newToken, err
}
