package utils

import (
	"github.com/bwmarrin/discordgo"
)

// EmbedSuccess returns a *MessageEmbed for a success message
func EmbedSuccess(content string) *discordgo.MessageEmbed {
	embed := &discordgo.MessageEmbed{
		Author:      &discordgo.MessageEmbedAuthor{},
		Color:       1022555,
		Title:       "Success!",
		Description: content}

	return embed
}

// EmbedFailure returns a *MessageEmbed for a failure message
func EmbedFailure(content string) *discordgo.MessageEmbed {
	embed := &discordgo.MessageEmbed{
		Author:      &discordgo.MessageEmbedAuthor{},
		Color:       16711684,
		Title:       "Failure!",
		Description: content}

	return embed
}
