package kbcmd

import (
	"github.com/bwmarrin/discordgo"
)

// Ping replies to a message with "Pong!"
func Ping(s *discordgo.Session, m *discordgo.MessageCreate, args []string) {
	s.ChannelMessageSend(m.ChannelID, "Pong!")
}
