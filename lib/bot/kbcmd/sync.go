package kbcmd

import (
	"kalbot/lib/api/models"
	"kalbot/lib/bot/api"
	log "sirupsaen/logrus"

	"github.com/bwmarrin/discordgo"
)

func Sync(s *discordgo.Session, m *discordgo.MessageCreate, args []string) {
	guildId := m.GuildID
	discordGuild, err := s.Guild(guildId)
	if err != nil {
		log.Error("Failed to retrieve guild from Discord API")
		return
	}

	apiGuild, err := api.GetGuildById(guildId)
	if err != nil {
		log.Error("Failled to retrieve guild from API")
		return
	}

	// Grab the useful bits we need and insert in the API
	//guild.GuildID = g.ID
	apiGuild.Icon = discordGuild.Icon
	apiGuild.Region = discordGuild.Region
	apiGuild.OwnerID = discordGuild.OwnerID
	apiGuild.MemberCount = discordGuild.MemberCount

	var availableRoles []models.Role
	for _, role := range discordGuild.Roles {
		availableRoles = append(availableRoles, models.Role{
			ID:          role.ID,
			Name:        role.Name,
			Color:       role.Color,
			Position:    role.Position,
			Permissions: role.Permissions,
			Managed:     role.Managed,
			Mentionable: role.Mentionable,
		})
	}
	guild.AvailableRoles = availableRoles

	err = api.UpdateGuild(guild)
	if err != nil {
		log.Error("Failed to update guild "+guild.guildID+" in API:, ", err)
		s.ChannelMessageSend(m.ChannelID, "Failed to sync guild. Try again later")
		return
	}

	s.ChannelMessageSend(m.ChannelId, "Sucessfully sync'd server info to API!")
}
