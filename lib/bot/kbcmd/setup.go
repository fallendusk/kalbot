package kbcmd

import (
	"kalbot/lib/bot/api"

	"github.com/bwmarrin/discordgo"
)

// Setup is handler that processes setup subcommands and calls the appropiate function
func Setup(s *discordgo.Session, m *discordgo.MessageCreate, args []string) {

}

func verifySetup(s *discordgo.Session, m *discordgo.MessageCreate, args []string) error {
	guild, err := api.GetGuildByID(m.GuildID)
	var setupErrors []string

	if guild.AuthMode == "" {
		setupErrors = append(setupErrors, "Auth Mode is not set")
	}

	if guild.AuthChannel == "" {
		setupErrors = append(setupErrors, "Auth Channel is not set")
	}

	if guild.DefaultRole == "" {
		setupErrors = append(setupErrors, "Default Role is not set")
	}

	// if guild.AdminRoles {
	// 	setupErrors = append(setupErrors, "No roles marked as admin")
	// }

	if guild.AnnounceChannel == "" {
		setupErrors = append(setupErrors, "Announcement channel is not set")
	}

	if guild.ServerName == "" {
		setupErrors = append(setupErrors, "Server name is not set")
	}

	return err
}
