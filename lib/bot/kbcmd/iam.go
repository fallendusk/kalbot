package kbcmd

import (
	"fmt"
	"kalbot/lib/bot/api"
	"kalbot/lib/bot/utils"
	"strings"

	"github.com/bwmarrin/discordgo"
)

// Iam authenticates a user, setting the default role and changing their nickname
func Iam(s *discordgo.Session, m *discordgo.MessageCreate, args []string) {
	guild, err := api.GetGuildByID(m.GuildID)
	if err != nil {
		fmt.Println("Failed to retrieve guild from API", err)
		return
	}

	if guild.AuthMode != "ffxiv" {
		fmt.Println("Auth disabled for guild ", guild.GuildID)
		return
	}

	if len(args) < 3 {
		s.ChannelMessageSendEmbed(m.ChannelID, utils.EmbedFailure("Missing argument. Please use k!iam "+guild.ServerName+" firstname lastname"))
		return
	}
	// Build Character name
	characterFirstName := strings.Title(args[1])
	characterLastName := strings.Title(args[2])
	characterName := characterFirstName + " " + characterLastName

	// Attempt to change nickname
	err = s.GuildMemberNickname(m.GuildID, m.Author.ID, characterName)
	if err != nil {
		fmt.Println("Failed to change nickname for ", m.Author.Username)
		fmt.Println(err)
		s.ChannelMessageSendEmbed(m.ChannelID, utils.EmbedFailure(err.Error()))
		return
	}

	// Add default role to member
	dguild, err := s.Guild(m.GuildID)
	if err != nil {
		fmt.Println(err)
		return
	}

	role := getGuildRoleByName(guild.DefaultRole, dguild)
	err = s.GuildMemberRoleAdd(m.GuildID, m.Author.ID, role)
	if err != nil {
		fmt.Println("Failed to add role " + guild.DefaultRole + " to " + m.Author.Username)
		fmt.Println(err)
		s.ChannelMessageSendEmbed(m.ChannelID, utils.EmbedFailure(err.Error()))
		return
	}

	s.ChannelMessageSendEmbed(m.ChannelID, utils.EmbedSuccess("<@"+m.Author.ID+"> authenticated as **"+characterName+"**"))

}

func getGuildRoleByName(name string, guild *discordgo.Guild) string {
	for _, role := range guild.Roles {
		if role.Name == name {
			return role.ID
		}
	}
	return ""
}
