package api

import (
	"bytes"
	"encoding/json"
	"kalbot/lib/api/models"
	"net/http"
)

var (
	apiBaseURL = "http://localhost:8081/"
)

func InsertGuild(guild models.Guild) error {
	url := apiBaseURL + "guilds"
	payload := new(bytes.Buffer)
	err := json.NewEncoder(payload).Encode(guild)
	if err != nil {
		return err
	}

	// POST JSON to API and get response
	res, err := http.Post(url, "application/json; charset=utf-8", payload)
	if err != nil {
		return err
	}

	// check that the response from server was a created guild
	var validGuild models.Guild
	if err := json.NewDecoder(res.Body).Decode(&validGuild); err != nil {
		return err
	}
	return nil
}

func GetGuildByID(id string) (guild models.Guild, err error) {
	// Build the request
	url := apiBaseURL + "guilds/" + id
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return guild, err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return guild, err
	}

	defer resp.Body.Close()

	if err := json.NewDecoder(resp.Body).Decode(&guild); err != nil {
		return guild, err
	}

	return guild, nil
}
