package controllers

import (
	"encoding/json"
	"net/http"

	"kalbot/lib/api/handler"
	"kalbot/lib/api/models"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

// GetUsers returns json of all users
func GetUsers(env *handler.Env, w http.ResponseWriter, r *http.Request) error {
	users, err := env.DB.UserFindAll()
	if err != nil {
		return handler.StatusError{500, err}
	}

	handler.RespondWithJSON(w, http.StatusOK, users)
	return nil
}

// FindUser returns json for a single user
func FindUser(env *handler.Env, w http.ResponseWriter, r *http.Request) error {
	params := mux.Vars(r)
	user, err := env.DB.UserFindByID(params["id"])

	if err != nil {
		return handler.StatusError{http.StatusBadRequest, err}
	}

	handler.RespondWithJSON(w, http.StatusOK, user)
	return nil
}

// CreateUser creates a new user
func CreateUser(env *handler.Env, w http.ResponseWriter, r *http.Request) error {
	defer r.Body.Close()
	var user models.User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		return handler.StatusError{http.StatusBadRequest, err}
	}

	user.ID = bson.NewObjectId()
	if err := env.DB.UserInsert(user); err != nil {
		return handler.StatusError{http.StatusInternalServerError, err}
	}

	handler.RespondWithJSON(w, http.StatusCreated, user)
	return nil
}

// DeleteUser deletes a user
func DeleteUser(env *handler.Env, w http.ResponseWriter, r *http.Request) error {
	defer r.Body.Close()
	var user models.User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		return handler.StatusError{http.StatusBadRequest, err}
	}

	if err := env.DB.UserDelete(user); err != nil {
		return handler.StatusError{http.StatusInternalServerError, err}
	}

	handler.RespondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
	return nil
}

// UpdateUser modifies an existing user
func UpdateUser(env *handler.Env, w http.ResponseWriter, r *http.Request) error {
	defer r.Body.Close()
	var user models.User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		return handler.StatusError{http.StatusBadRequest, err}
	}
	if err := env.DB.UserUpdate(user); err != nil {
		return handler.StatusError{http.StatusInternalServerError, err}
	}

	handler.RespondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
	return nil
}
