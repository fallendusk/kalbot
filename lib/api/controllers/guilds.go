package controllers

import (
	"encoding/json"
	"net/http"

	"kalbot/lib/api/handler"
	"kalbot/lib/api/models"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

// GetGuilds returns json of all guilds
func GetGuilds(env *handler.Env, w http.ResponseWriter, r *http.Request) error {
	guilds, err := env.DB.GuildFindAll()
	if err != nil {
		return handler.StatusError{500, err}
	}

	handler.RespondWithJSON(w, http.StatusOK, guilds)
	return nil
}

// FindGuild returns json for a single guild
func FindGuild(env *handler.Env, w http.ResponseWriter, r *http.Request) error {
	params := mux.Vars(r)
	guild, err := env.DB.GuildFindByID(params["id"])

	if err != nil {
		return handler.StatusError{http.StatusBadRequest, err}
	}

	handler.RespondWithJSON(w, http.StatusOK, guild)
	return nil
}

// CreateGuild creates a new guild
func CreateGuild(env *handler.Env, w http.ResponseWriter, r *http.Request) error {
	defer r.Body.Close()
	var guild models.Guild
	if err := json.NewDecoder(r.Body).Decode(&guild); err != nil {
		return handler.StatusError{http.StatusBadRequest, err}
	}

	guild.ID = bson.NewObjectId()
	if err := env.DB.GuildInsert(guild); err != nil {
		return handler.StatusError{http.StatusInternalServerError, err}
	}

	handler.RespondWithJSON(w, http.StatusCreated, guild)
	return nil
}

// DeleteGuild deletes a guild
func DeleteGuild(env *handler.Env, w http.ResponseWriter, r *http.Request) error {
	defer r.Body.Close()
	var guild models.Guild
	if err := json.NewDecoder(r.Body).Decode(&guild); err != nil {
		return handler.StatusError{http.StatusBadRequest, err}
	}

	if err := env.DB.GuildDelete(guild); err != nil {
		return handler.StatusError{http.StatusInternalServerError, err}
	}

	handler.RespondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
	return nil
}

// UpdateGuild modifies an existing guild
func UpdateGuild(env *handler.Env, w http.ResponseWriter, r *http.Request) error {
	defer r.Body.Close()
	var guild models.Guild
	if err := json.NewDecoder(r.Body).Decode(&guild); err != nil {
		return handler.StatusError{http.StatusBadRequest, err}
	}
	if err := env.DB.GuildUpdate(guild); err != nil {
		return handler.StatusError{http.StatusInternalServerError, err}
	}

	handler.RespondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
	return nil
}
