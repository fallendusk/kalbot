package dao

import (
	"kalbot/lib/api/models"

	"gopkg.in/mgo.v2/bson"
)

// GuildFindAll finds all guilds
func (m *DAO) GuildFindAll() ([]models.Guild, error) {
	var guilds []models.Guild
	err := db.C("guilds").Find(bson.M{}).All(&guilds)
	return guilds, err
}

// GuildFindByID finds a specific guild by ID
func (m *DAO) GuildFindByID(id string) (models.Guild, error) {
	var guild models.Guild
	err := db.C("guilds").Find(bson.M{"guildid": id}).One(&guild)
	return guild, err
}

// GuildInsert create a new guild
func (m *DAO) GuildInsert(guild models.Guild) error {
	err := db.C("guilds").Insert(&guild)
	return err
}

// GuildDelete removes an existing guild
func (m *DAO) GuildDelete(guild models.Guild) error {
	err := db.C("guilds").Remove(&guild)
	return err
}

// GuildUpdate updates an existing guild
func (m *DAO) GuildUpdate(guild models.Guild) error {
	err := db.C("guilds").UpdateId(guild.ID, &guild)
	return err
}
