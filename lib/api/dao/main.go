package dao

import (
	"log"

	mgo "gopkg.in/mgo.v2"
)

type DAO struct {
	Server   string
	Database string
}

var db *mgo.Database

// Connect establishes connection to db
func (m *DAO) Connect() {
	session, err := mgo.Dial(m.Server)
	if err != nil {
		log.Fatal(err)
	}
	db = session.DB(m.Database)

	// Setup indexes if they dont exist
	guildIndex := mgo.Index{
		Key:    []string{"guildid"},
		Unique: true,
	}
	userIndex := mgo.Index{
		Key:    []string{"userid", "guildid"},
		Unique: true,
	}

	err = db.C("guilds").EnsureIndex(guildIndex)
	if err != nil {
		log.Fatal(err)
	}
	err = db.C("users").EnsureIndex(userIndex)
	if err != nil {
		log.Fatal(err)
	}
}
