package dao

import (
	"kalbot/lib/api/models"

	"gopkg.in/mgo.v2/bson"
)

// UserFindAll finds all users
func (m *DAO) UserFindAll() ([]models.User, error) {
	var users []models.User
	err := db.C("users").Find(bson.M{}).All(&users)
	return users, err
}

// UserFindByID finds a specific user by ID
func (m *DAO) UserFindByID(id string) (models.User, error) {
	var user models.User
	err := db.C("users").Find(bson.M{"userid": id}).One(&user)
	return user, err
}

// UserInsert create a new user
func (m *DAO) UserInsert(user models.User) error {
	err := db.C("users").Insert(&user)
	return err
}

// UserDelete removes an existing user
func (m *DAO) UserDelete(user models.User) error {
	err := db.C("users").Remove(&user)
	return err
}

// UserUpdate updates an existing user
func (m *DAO) UserUpdate(user models.User) error {
	err := db.C("users").UpdateId(user.ID, &user)
	return err
}
