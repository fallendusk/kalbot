package models

import "gopkg.in/mgo.v2/bson"

// Character struct
type Character struct {
	ID     bson.ObjectId `bson:"_id" json:"id,omitempty"`
	Name   string        `json:"name,omitempty"`
	Server string        `json:"server,omitempty"`
	Type   string        `json:"type,omitempty"`
}
