package models

import (
	"gopkg.in/mgo.v2/bson"
)

// Guild stuct
type Guild struct {
	// ID of the Guild in Mongo
	ID bson.ObjectId `bson:"_id" json:"id,omitempty"`

	// GuildID is the ID of the guild in the discord API
	GuildID     string `json:"guild_id,omitempty"`
	Icon        string `json:"icon,omitempty"`
	Region      string `json:"region,omitempty"`
	OwnerID     string `json:"region,omitempty"`
	MemberCount int    `json:"region,omitempty"`

	// AvailableRoles array of all Roles of the guild
	AvailableRoles []Role `json:"roles,omitempty"`

	// Plugin settings : Authentication
	// AuthChannel string representation of the channel used for Auth
	AuthChannel string `json:"auth_channel,omitempty"`

	// AuthMode name of the auth mode provider
	AuthMode string `json:"auth_mode,omitempty"`

	// DefaultRole name of default role to place authenticated users in
	DefaultRole string `json:"default_role,omitempty"`

	// Plugin settings : Events
	// TODO Remove or rename this? For events plugin
	// AdminRoles array of role names that are allowed to access admin functions
	AdminRoles []string `json:"admin_roles,omitempty"`

	// TODO Remove or rename this? Was used for event reminder announcements
	// AnnounceChannel Channel to send announcements to
	AnnounceChannel string `json:"announce_channel,omitempty"`

	// TODO Remove this? Was only used for generating the welcome message
	// which is now fully user customizable
	// ServerName name of game server for auth welcome instructions
	ServerName string `json:"server_name,omitempty"`
}

// Role struct
type Role struct {
	// ID of the role
	ID string `json:"id,omitempty"`
	// Name of the role
	Name string `json:"name,omitempty"`
	// Color the integer representation of hexadecimal color code
	Color int `json:"color,omitempty"`
	// Position of the role
	Position int `json:"position,omitempty"`
	// Permissions the permission bit set of this role
	Permissions int `json:"permissions,omitempty"`
	// Managed whether this role is managed by an intergration
	Managed bool `json:"managed,omitempty"`
	// Mentionable whether this role is mentionable
	Mentionable bool `json:"mentionable,omitempty"`
}
