package models

import "gopkg.in/mgo.v2/bson"

// User struct
type User struct {
	ID        bson.ObjectId `bson:"_id" json:"id,omitempty"`
	UserID    string        `json:"user_id,omitempty"`
	GuildID   string        `json:"guild_id,omitempty"`
	Character Character     `json:"character,omitempty"`
}
