package api

import (
	"kalbot/lib/api/models"
)

type User struct{}

func (self User) FindById(id string) (models.User, error) {
	var user models.User
	user.UserID = id
	return user, nil
}
