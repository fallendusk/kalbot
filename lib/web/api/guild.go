package api

import (
	"bytes"
	"encoding/json"
	"kalbot/lib/api/models"
	"net/http"
)

// Guild struct (namespace)
type Guild struct{}

// FindByID retrives a guild from the API by id
func (g Guild) FindByID(id string) (guild models.Guild, err error) {
	url := BaseURL + "guilds/" + id

	res, err := http.Get(url)
	if err != nil {
		return guild, err
	}
	err = json.NewDecoder(res.Body).Decode(&guild)
	if err != nil {
		return guild, err
	}

	return guild, nil
}

// FindAll retrieves all guilds from the API
func (g Guild) FindAll() (guilds []models.Guild, err error) {
	url := BaseURL + "guilds/"

	res, err := http.Get(url)
	if err != nil {
		return guilds, err
	}

	err = json.NewDecoder(res.Body).Decode(&guilds)
	if err != nil {
		return guilds, err
	}

	return guilds, err
}

// Create creates a new guild on the API
func (g Guild) Create(guild models.Guild) (models.Guild, error) {
	url := BaseURL + "guilds"
	payload := new(bytes.Buffer)
	err := json.NewEncoder(payload).Encode(guild)
	if err != nil {
		return guild, err
	}

	res, err := http.Post(url, "application/json; charset=utf-8", payload)
	if err != nil {
		return guild, err
	}

	var validGuild models.Guild
	if err := json.NewDecoder(res.Body).Decode(&validGuild); err != nil {
		return guild, err
	}
	return validGuild, nil
}

// Delete removes a guild from the API
func (g Guild) Delete(guild models.Guild) error {
	return nil
}

// Update replaces the guild on the API with a new version
func (g Guild) Update(guild models.Guild) error {
	return nil
}
