package controllers

import (
	"kalbot/lib/api/models"
	discord "kalbot/lib/discord-oauth"
	"kalbot/lib/web/framework"
	"kalbot/lib/web/views"
	"net/http"

	"github.com/gorilla/mux"
)

// GuildController inherits framework.Controller
type GuildController struct {
	framework.Controller
}

// GuildTemplateData is a struct containing data to be parsed into a template
type GuildTemplateData struct {
	LoggedIn     bool
	User         discord.User
	APIGuild     models.Guild
	DiscordGuild discord.Guild
}

// Guild is a shortcut to allow calling GuildController methods from the router
var Guild GuildController

var inviteLink = "https://discordapp.com/oauth2/authorize?&client_id=410745640923168779&scope=bot&permissions=469838848"

// Show the dashboard for a guild
func (c GuildController) Show(env *framework.Env, w http.ResponseWriter, r *http.Request) error {
	// Figure out if user is allowed to view this resource
	isLoggedIn, user := c.DoUserAuth(w, r)
	if !isLoggedIn {
		c.RedirectToIndex(w)
		return nil
	}

	var guild discord.Guild
	params := mux.Vars(r)
	guildID := params["id"]
	canAdminGuild := false
	for _, g := range user.Guilds {
		if g.ID == guildID && g.Admin {
			canAdminGuild = true
			guild = g
		}
	}

	if !canAdminGuild {
		c.RedirectToIndex(w)
		return nil
	}

	// Get Guild data from the API
	apiGuild, err := env.DAO.Guild.FindByID(guildID)
	if err != nil {
		// TODO Check the actual error to see if guild doesn't exist yet or something else happen
		w.Header().Set("Location", inviteLink+"&guild_id="+guildID)
		w.WriteHeader(http.StatusTemporaryRedirect)
		return nil
	}

	// Render the view
	data := GuildTemplateData{
		LoggedIn:     isLoggedIn,
		User:         user,
		APIGuild:     apiGuild,
		DiscordGuild: guild,
	}
	err = views.Guild.Show.RenderView(w, data)
	if err != nil {
		return framework.StatusError{500, err}
	}
	return nil
}
