package controllers

import (
	"kalbot/lib/web/framework"
	"net/http"

	"kalbot/lib/web/gothic"

	log "github.com/sirupsen/logrus"
)

type AuthController struct {
	framework.Controller
}

var Auth AuthController

func (c AuthController) Callback(env *framework.Env, w http.ResponseWriter, r *http.Request) error {
	_, err := gothic.CompleteUserAuth(w, r)
	if err != nil {
		return framework.StatusError{500, err}
	}

	c.RedirectToIndex(w)
	return nil
}

func (c AuthController) Logout(env *framework.Env, w http.ResponseWriter, r *http.Request) error {
	gothic.Logout(w, r)
	c.RedirectToIndex(w)
	return nil
}

func (c AuthController) Index(env *framework.Env, w http.ResponseWriter, r *http.Request) error {
	// try to get the user without re-authenticating
	user, err := gothic.CompleteUserAuth(w, r)
	if err != nil {
		gothic.BeginAuthHandler(w, r)
	} else {
		c.RedirectToIndex(w)
		log.Trace("User logged in: ", user)
	}
	return nil
}
