package controllers

import (
	discord "kalbot/lib/discord-oauth"
	"kalbot/lib/web/framework"
	"kalbot/lib/web/views"
	"net/http"
)

type HomeController struct {
	framework.Controller
}

type HomeTemplateData struct {
	LoggedIn bool
	User     discord.User
}

var Home HomeController

func (c HomeController) Index(env *framework.Env, w http.ResponseWriter, r *http.Request) error {
	isLoggedIn, user := c.DoUserAuth(w, r)

	data := HomeTemplateData{LoggedIn: isLoggedIn, User: user}
	err := views.Home.Index.RenderView(w, data)
	if err != nil {
		return framework.StatusError{500, err}
	}
	return nil
}
