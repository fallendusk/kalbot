package framework

import (
	"html/template"
	"log"
	"net/http"
	"path/filepath"
)

// LayoutFiles returns a list of global layout templates
func LayoutFiles() []string {
	files, err := filepath.Glob("templates/layouts/*.tmpl")
	if err != nil {
		log.Panic(err)
	}
	return files
}

// View struct contains all the pages for a controller's views
type View struct {
	Index  Page
	Show   Page
	New    Page
	Create Page
	Edit   Page
	Update Page
	Delete Page
}

// Page struct
type Page struct {
	Template *template.Template
	Layout   string
}

// RenderView renders a view
func (p *Page) RenderView(w http.ResponseWriter, data interface{}) error {
	return p.Template.ExecuteTemplate(w, p.Layout, data)
}

func getTemplateIncludeFiles(view string) []string {
	files, err := filepath.Glob("templates/" + view + "/includes/*.tmpl")
	if err != nil {
		log.Fatal(err)
	}

	files = append(files, LayoutFiles()...)
	return files
}

// PrepareView returns a Page based on layout, view, and template name passed
func PrepareView(layout string, view string, tmpl string) Page {
	files := append(getTemplateIncludeFiles(view), "templates/"+view+"/"+tmpl+".html.tmpl")
	return Page{
		Template: template.Must(template.New(tmpl).ParseFiles(files...)),
		Layout:   layout,
	}
}
