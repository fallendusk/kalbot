package framework

import (
	discord "kalbot/lib/discord-oauth"
	"kalbot/lib/web/api"
	"kalbot/lib/web/gothic"
	"net/http"

	log "github.com/sirupsen/logrus"
)

// Env is passed to every controller method, contains a pointer to the api DAO
type Env struct {
	DAO *api.DAO
}

// Controller struct (namespace)
type Controller struct {
}

// Handler struct that takes a configured Env and a function matching
// our useful signature.
type Handler struct {
	*Env
	H func(e *Env, w http.ResponseWriter, r *http.Request) error
}

// Error represents a handler error. It provides methods for a HTTP status
// code and embeds the built-in error interface.
type Error interface {
	error
	Status() int
}

// StatusError represents an error with an associated HTTP status code.
type StatusError struct {
	Code int
	Err  error
}

// Error Allows StatusError to satisfy the error interface.
func (se StatusError) Error() string {
	return se.Err.Error()
}

// Status Returns our HTTP status code.
func (se StatusError) Status() int {
	return se.Code
}

// ServeHTTP allows our Handler type to satisfy http.Handler.
func (h Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := h.H(h.Env, w, r)
	if err != nil {
		switch e := err.(type) {
		case Error:
			// We can retrieve the status here and write out a specific
			// HTTP status code.
			log.WithFields(log.Fields{"status": e.Status()}).Error("HTTP Error: ", e.Error())
			http.Error(w, e.Error(), e.Status())
		default:
			// Any error types we don't specifically look out for default
			// to serving a HTTP 500
			log.WithFields(log.Fields{"status": 500}).Error("HTTP Error: ", e.Error())
			http.Error(w, http.StatusText(http.StatusInternalServerError),
				http.StatusInternalServerError)
		}
	}
}

// DoUserAuth passes authentication to discord-oauth and returns a bool whether user is logged in
// along with a discord user
func (c Controller) DoUserAuth(w http.ResponseWriter, r *http.Request) (bool, discord.User) {
	user, err := gothic.CompleteUserAuth(w, r)

	if err != nil {
		log.Trace("Login failed: ", err)
		return false, user

	}
	return true, user
}

// RedirectToIndex redirects the client to the index page
func (c Controller) RedirectToIndex(w http.ResponseWriter) {
	w.Header().Set("Location", "/")
	w.WriteHeader(http.StatusTemporaryRedirect)
}
