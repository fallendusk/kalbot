package views

import (
	"kalbot/lib/web/framework"
)

type HomeView struct {
	framework.View
}

type GuildView struct {
	framework.View
}

var Home HomeView
var Guild GuildView

func init() {
	// index view
	Home.Index = framework.PrepareView("application", "home", "index")
	Guild.Show = framework.PrepareView("application", "guild", "show")
}
