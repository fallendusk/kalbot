module kalbot

require (
	github.com/bwmarrin/discordgo v0.19.0
	github.com/google/pprof v0.0.0-20190228041337-2ef8d84b2e3c // indirect
	github.com/gorilla/handlers v1.4.0
	github.com/gorilla/mux v1.7.0
	github.com/gorilla/sessions v1.1.3
	github.com/ianlancetaylor/demangle v0.0.0-20181102032728-5e5cf60278f6 // indirect
	github.com/sirupsen/logrus v1.3.0
	golang.org/x/arch v0.0.0-20190226203302-36aee92af9e8 // indirect
	golang.org/x/oauth2 v0.0.0-20190226205417-e64efc72b421
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce
)
