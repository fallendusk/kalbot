BIN_DIR := ./bin
GOARCH := amd64
GOOS := linux

.PHONY: all
all: api web bot

.PHONY: api
api:
	GOOS=$(GOOS) GOARCH=$(GOARCH) go build -o $(BIN_DIR)/kb-api cmd/kb-api/main.go

.PHONY: web
web:
	GOOS=$(GOOS) GOARCH=$(GOARCH) go build -o $(BIN_DIR)/kb-web cmd/kb-web/main.go

.PHONY: bot
bot:
	GOOS=$(GOOS) GOARCH=$(GOARCH) go build -o $(BIN_DIR)/kb-bot cmd/kb-bot/main.go
.PHONY: clean
clean:
	rm -rf ./bin
.PHONY: buildall
buildall:
	go build ./...

