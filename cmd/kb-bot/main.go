package main

import (
	"flag"
	"fmt"
	"kalbot/lib/api/models"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/bwmarrin/discordgo"
	log "github.com/sirupsen/logrus"

	"kalbot/lib/bot/api"
	"kalbot/lib/bot/kbcmd"
)

var (
	// Token bot token
	Token  string
	prefix = "k!"
)

func init() {
	flag.StringVar(&Token, "t", "", "Bot Token")
	flag.Parse()
}

func main() {
	// Create new discord session using provided token
	dg, err := discordgo.New("Bot " + Token)
	if err != nil {
		fmt.Println("Error creating Discord session, ", err)
		return
	}

	// Register functions as callback for discord events
	dg.AddHandler(messageCreate)
	dg.AddHandler(guildOnCreate)
	dg.AddHandler(guildOnDelete)

	// Connect to Discord
	err = dg.Open()
	if err != nil {
		fmt.Println("Error connection to Discord WebSocket, ", err)
		return
	}

	// TODO : Change this to hit a heartbeat endpoint on the API
	//guild, err := api.GetGuildByID("488407645410033674")
	//if err != nil {
	//log.Fatal("Error testing API, is it running?\n", err)
	//}
	//fmt.Println(guild.GuildID)

	err = dg.UpdateStatus(0, "Made with go <3")
	if err != nil {
		fmt.Println("Failed to set activity", err)
	}
	fmt.Println("Kalbot connected to Discord! Press CTRL-C to exit.")
	//fmt.Printf("Serving %d users, in %d channels of %d guilds", len(connections), len(channels), len(guilds))
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	dg.Close()
}

func guildOnCreate(s *discordgo.Session, g *discordgo.GuildCreate) {
	var guild models.Guild
	// Check if guild already exists
	guild, err := api.GetGuildByID(g.ID)
	if err == nil {
		// Guild ID already existed
		fmt.Println("DEBUG: Guild " + g.ID + " already exists in API")
		return
	}

	if g.Unavailable == true {
		log.Trace("Guild unavailable and no match in API found!")
		return
	}

	// Grab the useful bits we need and insert in the API
	guild.GuildID = g.ID
	guild.Icon = g.Icon
	guild.Region = g.Region
	guild.OwnerID = g.OwnerID
	guild.MemberCount = g.MemberCount

	var availableRoles []models.Role
	for _, role := range g.Roles {
		availableRoles = append(availableRoles, models.Role{
			ID:          role.ID,
			Name:        role.Name,
			Color:       role.Color,
			Position:    role.Position,
			Permissions: role.Permissions,
			Managed:     role.Managed,
			Mentionable: role.Mentionable,
		})
	}
	guild.AvailableRoles = availableRoles
	err = api.InsertGuild(guild)
	if err != nil {
		fmt.Println("Failed to insert guild "+g.ID+" into API: ", err)
		// How do we fail this gracefully, send message to guild saying an error has occured
		// and the bot needs to reinvited?
		return
	}
	fmt.Println("Added new guild " + g.ID + " to API")
	// TODO : send welcome message to guild
}

func guildOnDelete(s *discordgo.Session, g *discordgo.GuildDelete) {
	fmt.Println("NOTIMPL: guildOnDelete func fired")
	if g.Unavailable == true {
		// Don't remove from API, guild is unavailable and the bot hasn't been kicked
		return
	}
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Ignore messages created by bot itself
	if m.Author.ID == s.State.User.ID {
		return
	}

	if len(m.Content) > 1 && strings.HasPrefix(m.Content, prefix) {
		messageArray := strings.Split(m.Content, " ")
		command := strings.ToLower(strings.TrimPrefix(messageArray[0], prefix))
		fmt.Println("DEBUG: Cmd is ", command)
		args := messageArray[1:]
		fmt.Println("DEBUG: Args are ", args)

		switch command {
		case "ping":
			kbcmd.Ping(s, m, args)
		case "iam":
			kbcmd.Iam(s, m, args)
		case "sync":
			kbcmd.Sync(s, m, args)

		}
	}
}
