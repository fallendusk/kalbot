package main

import (
	"kalbot/lib/web/api"
	"kalbot/lib/web/controllers"
	"kalbot/lib/web/framework"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

var (
	PublicDirectory = "./assets/"
	port            = "3000"
	dao             = api.DAO{}
)

func main() {
	log.SetLevel(log.TraceLevel)
	env := &framework.Env{
		DAO: &dao,
	}
	router := mux.NewRouter()

	// Authentication routes
	router.Handle("/auth/callback", framework.Handler{env, controllers.Auth.Callback}).Methods("GET")
	router.Handle("/logout", framework.Handler{env, controllers.Auth.Logout}).Methods("GET")
	router.Handle("/auth", framework.Handler{env, controllers.Auth.Index}).Methods("GET")

	// Index routes
	router.Handle("/", framework.Handler{env, controllers.Home.Index}).Methods("GET")

	// Guild routes
	router.Handle("/guild/{id}", framework.Handler{env, controllers.Guild.Show}).Methods("GET")

	// Static assets handler
	router.PathPrefix("/assets/").Handler(http.StripPrefix("/assets", http.FileServer(http.Dir(PublicDirectory))))

	log.WithFields(log.Fields{"port": port}).Info("Kalbot-web now serving")
	if err := http.ListenAndServe(":"+port, handlers.LoggingHandler(os.Stdout, router)); err != nil {
		log.Fatal(err)
	}
}
