package main

import (
	"net/http"
	"os"

	"kalbot/lib/api/controllers"
	"kalbot/lib/api/dao"
	"kalbot/lib/api/handler"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

var db = dao.DAO{}

func init() {
	db.Server = "172.17.0.2"
	db.Database = "kalbot"
	db.Connect()
}

func main() {
	router := mux.NewRouter()
	env := &handler.Env{
		DB: &db,
	}

	// Define routes
	// Guilds
	router.Handle("/guilds", handler.Handler{env, controllers.GetGuilds}).Methods("GET")
	router.Handle("/guilds/{id}", handler.Handler{env, controllers.FindGuild}).Methods("GET")
	router.Handle("/guilds", handler.Handler{env, controllers.CreateGuild}).Methods("POST")
	router.Handle("/guilds", handler.Handler{env, controllers.DeleteGuild}).Methods("DELETE")
	router.Handle("/guilds", handler.Handler{env, controllers.UpdateGuild}).Methods("PUT")
	// Users
	router.Handle("/users", handler.Handler{env, controllers.GetUsers}).Methods("GET")
	router.Handle("/users/{id}", handler.Handler{env, controllers.FindUser}).Methods("GET")
	router.Handle("/users", handler.Handler{env, controllers.CreateUser}).Methods("POST")
	router.Handle("/users", handler.Handler{env, controllers.DeleteUser}).Methods("DELETE")
	router.Handle("/users", handler.Handler{env, controllers.UpdateUser}).Methods("PUT")

	log.Info("Kalbot-api serving on port 8081\n")

	if err := http.ListenAndServe(":8081", handlers.LoggingHandler(os.Stdout, router)); err != nil {
		log.Fatal("http.ListenAndServe: ", err)
	}
}
