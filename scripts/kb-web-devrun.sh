#!/bin/sh

# Trap INT so we can exit gracefully
trap ctrl_c INT
ctrl_c() {
       	echo "** Ctrl-C pressed, exiting... **"
        pkill kb-web
       	exit
}
while true; do
  make web
  ./bin/kb-web &
  PID=$!
  inotifywait -r -e modify ./lib/web ./templates
  kill $PID
done
